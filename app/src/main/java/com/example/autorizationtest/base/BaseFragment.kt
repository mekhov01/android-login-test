package com.example.autorizationtest.base


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.autorizationtest.R

abstract class BaseFragment<V : BasView, P : BasePresenter> : Fragment() {

    protected lateinit var presenter: P

    protected abstract fun createPresenter(): P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = createPresenter()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }
}
