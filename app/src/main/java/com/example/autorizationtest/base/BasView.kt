package com.example.autorizationtest.base

interface BasView {
    fun <T> onSuccess(data: T)
    fun showError(error: Int)
    fun hideError()
    fun showLoading()
    fun hideLoading()
}