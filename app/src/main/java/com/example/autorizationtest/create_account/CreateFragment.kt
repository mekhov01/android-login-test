package com.example.autorizationtest.create_account


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.autorizationtest.R
import com.example.autorizationtest.base.BaseFragment
import com.example.autorizationtest.utils.hideKeyBord
import com.example.autorizationtest.utils.onTextChanged
import com.example.autorizationtest.utils.string
import kotlinx.android.synthetic.main.fragment_create.*

class CreateFragment : BaseFragment<CreateAccountView, CreateAccountPresenter>(), CreateAccountView {

    override fun createPresenter() = CreateAccountPresImpl(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        repeat_edit.onTextChanged { presenter.isMatches(it, create_password_edit.text.toString()) }
        create_account.setOnClickListener { makeLogin() }
        create_email_edit.onTextChanged { hideError() }
        create_password_edit.onTextChanged { hideError() }
    }

    override fun <T> onSuccess(data: T) {

    }

    private fun makeLogin() {
        requireActivity().hideKeyBord()
        val email = create_email_edit.text.toString()
        val password = create_password_edit.text.toString()
        val repeat = repeat_edit.text.toString()
        presenter.makeLogIn(email, password, repeat)
    }

    override fun showError(error: Int) {
        error_text.text = requireContext().string(error)
        error_text.visibility = View.VISIBLE
    }

    override fun hideError() {
        error_text.text = ""
        error_text.visibility = View.GONE
    }

    override fun showLoading() {
        createProgress.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        createProgress.visibility = View.GONE
    }
}
