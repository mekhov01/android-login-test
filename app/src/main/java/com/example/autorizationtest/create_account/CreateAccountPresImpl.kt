package com.example.autorizationtest.create_account

import com.example.autorizationtest.R
import com.example.autorizationtest.utils.isEmailValid
import com.example.autorizationtest.utils.isPasswordValid

class CreateAccountPresImpl(private var view: CreateAccountView?) : CreateAccountPresenter {

    override fun makeLogIn(email: String, password: String, repeatedPassword: String) {
        if (!email.isEmailValid()) {
            view?.showError(R.string.invalid_email)
            return
        }
        if (!password.isPasswordValid()) {
            view?.showError(R.string.invalid_password)
            return
        }
        if (!repeatedPassword.equals(password)) {
            view?.showError(R.string.repeat_password_error)
            return
        }
    }

    override fun isMatches(it: String?, password: String) {
        it?.let {
            if (!it.equals(password)) {
                view?.showError(R.string.repeat_password_error)
            } else {
                view?.hideError()
            }
        }
    }

    override fun onDestroy() {
        view = null
    }
}