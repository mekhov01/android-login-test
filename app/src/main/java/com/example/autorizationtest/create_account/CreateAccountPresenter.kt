package com.example.autorizationtest.create_account

import com.example.autorizationtest.base.BasePresenter

interface CreateAccountPresenter : BasePresenter {
    fun makeLogIn(email: String, password: String, repeatedPassword: String)
    fun isMatches(it: String?, password: String)
}