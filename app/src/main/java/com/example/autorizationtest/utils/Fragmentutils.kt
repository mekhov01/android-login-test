package com.example.autorizationtest.utils

import androidx.fragment.app.Fragment
import com.example.autorizationtest.R

fun Fragment.openFragment(container: Int = R.id.main_container, fragmentManager: androidx.fragment.app.FragmentManager?,
                          whitBackStack: Boolean = true, isAnimated: Boolean = true,
                          openType: FOpenType = FOpenType.REPLACE, tag: FragmentTag?) {
    val transaction = fragmentManager?.beginTransaction()
    if (isAnimated)transaction?.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up,
        R.anim.slide_in_down, R.anim.slide_out_down)
    else transaction?.setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE)
    if (openType == FOpenType.ADD)transaction?.add(container, this, tag?.name)
    else transaction?.replace(container, this, tag?.name)
    if (whitBackStack)transaction?.addToBackStack(tag?.name)
    transaction?.commit()
}
