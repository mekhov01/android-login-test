package com.example.autorizationtest.utils

enum class FragmentTag(tag: String) {
    LOGIN_FRAGMENT("Login Fragment"),

    CREATE_ACCOUNT_FRAGMENT("Create Account"),

    CHANGE_PASSWORD("change password")

}
enum class FOpenType {
    ADD,
    REPLACE
}