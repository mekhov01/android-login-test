package com.example.autorizationtest.utils

import android.app.Activity
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.StringRes
import java.util.regex.Pattern

fun Activity.hideKeyBord() {
    val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = this.getCurrentFocus()
    if (view == null) {
        view = View(this)
    }
    imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
}

fun String.isEmailValid(): Boolean {
    if (android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()) {
        return true
    } else return false
}

fun String.isPasswordValid(): Boolean {
    val pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#\$%!\\-_?&])(?=\\S+\$).{6,}".toRegex()
    return pattern.matches(this)
}

fun EditText.onFocusChanged(onChanged: (String?) -> Unit) {
    setOnFocusChangeListener(object : View.OnFocusChangeListener {
        override fun onFocusChange(v: View?, hasFocus: Boolean) {
            onChanged(toString())
        }
    })
}

fun EditText.onTextChanged(onChanged: (String?) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            onChanged(s.toString())
        }
    })
}

fun EditText.dismissKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

fun Context.makeToast(mesage: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, mesage, duration).show()
}

fun Context.string(@StringRes str: Int): String {
    return getString(str)
}
