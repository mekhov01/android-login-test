package com.example.autorizationtest.forget_password


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.autorizationtest.R
import com.example.autorizationtest.base.BaseFragment
import com.example.autorizationtest.utils.hideKeyBord
import com.example.autorizationtest.utils.onTextChanged
import com.example.autorizationtest.utils.string
import kotlinx.android.synthetic.main.fragment_forget_password.*

class ForgetPasswordFragment : BaseFragment<ForgetView, ForgetPresenter>(), ForgetView {

    override fun createPresenter() = ForgetPasPresImpl(this)
    lateinit var callBack: ChangePasswordCallBack

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        callBack = targetFragment as ChangePasswordCallBack
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_forget_password, container, false)
    }

    override fun <T> onSuccess(data: T) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        change_password.setOnClickListener { changePassword() }
        email_edit.onTextChanged { hideError() }
    }

    private fun changePassword() {
        requireActivity().hideKeyBord()
        val email = email_edit.text.toString()
        presenter.changePassword(email)
    }

    override fun showError(error: Int) {
        error_text.text = requireContext().string(error)
        error_text.visibility = View.VISIBLE
    }

    override fun hideError() {
        error_text.text = ""
        error_text.visibility = View.GONE
    }

    override fun showLoading() {
        forgetProgress.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        forgetProgress.visibility = View.GONE
    }

    override fun onDestroy() {

        super.onDestroy()
    }

    interface ChangePasswordCallBack {
        fun changed(pasword: String)
    }
}
