package com.example.autorizationtest.forget_password

import com.example.autorizationtest.base.BasePresenter

interface ForgetPresenter : BasePresenter {
    fun changePassword(email: String)

}