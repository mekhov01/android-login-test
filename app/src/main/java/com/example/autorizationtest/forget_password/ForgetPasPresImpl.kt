package com.example.autorizationtest.forget_password

import com.example.autorizationtest.R
import com.example.autorizationtest.utils.isEmailValid

class ForgetPasPresImpl(private var view: ForgetView?) : ForgetPresenter {

    override fun changePassword(email: String) {
        view?.showLoading()
        if (!email.isEmailValid()) {
            view?.showError(R.string.invalid_email)
            return
        }
        view?.hideLoading()
    }

    override fun onDestroy() {
        view = null
    }
}