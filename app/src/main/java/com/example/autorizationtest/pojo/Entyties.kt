package com.example.autorizationtest.pojo

data class HeaderResponse(val main: Main)

data class Main(val temp: Double)