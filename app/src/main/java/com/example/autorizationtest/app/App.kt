package com.example.autorizationtest.app

import android.app.Application

class App : Application() {

    companion object{
        private lateinit var app: App
        fun getInstance() = app
    }

    override fun onCreate() {
        super.onCreate()
        app = this
    }

}