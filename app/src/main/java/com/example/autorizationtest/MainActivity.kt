package com.example.autorizationtest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.autorizationtest.create_account.CreateFragment
import com.example.autorizationtest.forget_password.ForgetPasswordFragment
import com.example.autorizationtest.login.LoginFragment
import com.example.autorizationtest.utils.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), LoginFragment.CommunicateCallBack {
    private val degreeToNorm: Float = 0F
    private val degreeToBottom: Float = -90F
    private val duration: Long = 150

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().add(R.id.main_container, LoginFragment()).commit()
        onBack.setOnClickListener {
            hideKeyBord()
            onBackPressed()
            create_account.visibility = View.VISIBLE
            create_account.isEnabled = true
            animateOnBack(degreeToNorm, duration)
        }
        create_account.setOnClickListener {
            toolTitle.text = string(R.string.registr_user)
            hideKeyBord()
            createAccount()
            animateOnBack(degreeToBottom, duration)
            create_account.visibility = View.INVISIBLE
            create_account.isEnabled = false
        }
    }

    /*called when us clicked forget password button*/
    override fun clicked() {
        toolTitle.text = string(R.string.reset_pass)
        animateOnBack(degreeToBottom, duration)
        create_account.visibility = View.INVISIBLE
        create_account.isEnabled = false
    }

    /*Manage app work when user clicked back button*/
    override fun onBackPressed() {
        toolTitle.text = string(R.string.login_title)
        val fragment = supportFragmentManager.findFragmentById(R.id.main_container)
        if (fragment is CreateFragment || fragment is ForgetPasswordFragment) {
            animateOnBack(degreeToNorm, duration)
            create_account.visibility = View.VISIBLE
            create_account.isEnabled = true
        }
        super.onBackPressed()
    }

    /*open fragment for creating new account*/
    private fun createAccount() {
        CreateFragment().openFragment(
            fragmentManager = supportFragmentManager,
            openType = FOpenType.ADD,
            tag = FragmentTag.CREATE_ACCOUNT_FRAGMENT
        )
    }

    private fun animateOnBack(value: Float, duration: Long) {
        onBack.animate().rotation(value).setDuration(duration)
    }
}
