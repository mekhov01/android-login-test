package com.example.autorizationtest.login

import android.util.Log
import com.example.autorizationtest.R
import com.example.autorizationtest.api.ApiService
import com.example.autorizationtest.pojo.HeaderResponse
import com.example.autorizationtest.utils.isEmailValid
import com.example.autorizationtest.utils.isPasswordValid
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresImpl(private var view: LoginView?) : LoginPresenter {

    /*Check if login and password valid make call*/
    override fun makeLogIn(email: String, password: String, client: ApiService) {
        if (!email.isEmailValid()) {
            view?.showError(R.string.invalid_email)
            return
        }
        if (!password.isPasswordValid()) {
            view?.showError(R.string.invalid_password)
            return
        }
        getWeather(client)
    }

    /*retrofit call get data from server if response is successful post weather data to the view else post error*/
    override fun getWeather(client: ApiService) {
        view?.showLoading()
        val call = client.getTemperature(55.7522, 37.6156, "e0eaa283b87793105b204feaa0b5a7d1")
        call.enqueue(object : Callback<HeaderResponse> {
            override fun onFailure(call: Call<HeaderResponse>, t: Throwable) {
                view?.hideLoading()
                t.message?.let {
                    view?.showReqEmail(it)
                }
            }

            override fun onResponse(call: Call<HeaderResponse>, response: Response<HeaderResponse>) {
                view?.hideLoading()
                view?.onSuccess(response.body())
            }
        })
    }

    /*destroy view interface for avoiding memory out  */
    override fun onDestroy() {
        view = null
    }
}