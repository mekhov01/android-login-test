package com.example.autorizationtest.login

import com.example.autorizationtest.base.BasView

interface LoginView : BasView {
    fun showReqEmail(error: String)
}