package com.example.autorizationtest.login

import com.example.autorizationtest.api.ApiService
import com.example.autorizationtest.base.BasePresenter

interface LoginPresenter : BasePresenter {
    fun makeLogIn(email: String, password: String, client: ApiService)

    fun getWeather(client: ApiService)
}