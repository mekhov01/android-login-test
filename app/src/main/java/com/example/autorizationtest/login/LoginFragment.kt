package com.example.autorizationtest.login


import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.example.autorizationtest.R
import com.example.autorizationtest.base.BaseFragment
import com.example.autorizationtest.forget_password.ForgetPasswordFragment
import com.example.autorizationtest.api.ApiService
import com.example.autorizationtest.api.RetrofitClient
import com.example.autorizationtest.pojo.HeaderResponse
import com.example.autorizationtest.utils.*
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_login.*

const val FORGET_CODE = 54;

class LoginFragment : BaseFragment<LoginView, LoginPresenter>(), LoginView,
    ForgetPasswordFragment.ChangePasswordCallBack {

    private lateinit var callBack: CommunicateCallBack

    override fun createPresenter() = LoginPresImpl(this)

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callBack = context as CommunicateCallBack
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        email_edit.onTextChanged { hideError() }
        password_edit.onTextChanged { hideError() }
        forget_password.setOnClickListener { forgetPassword() }
        val apiServiceJson = RetrofitClient.getClientJson().create(ApiService::class.java)
        log_in.setOnClickListener { makeLogin(apiServiceJson) }
    }

    override fun showLoading() {
        loginProgress.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loginProgress.visibility = View.GONE
    }

    /*Return result and show in Snac Bar when response is successful*/
    override fun <T> onSuccess(data: T) {
        val temp = data as HeaderResponse
        Snackbar.make(view!!, "${(temp.main.temp - 273.15).toInt()}C", Snackbar.LENGTH_LONG).show()
    }

    /*email and password checking if true call presenters login */
    private fun makeLogin(client: ApiService) {
        requireActivity().hideKeyBord()
        val email = email_edit.text.toString()
        val password = password_edit.text.toString()
        presenter.makeLogIn(email, password, client)
    }

    /*hide error when its null*/
    override fun hideError() {
        error_text.text = ""
        error_text.visibility = View.GONE
    }

    /*show error when its exist*/
    override fun showError(error: Int) {
        error_text.text = requireContext().string(error)
        error_text.visibility = View.VISIBLE
    }

    override fun showReqEmail(error: String) {
        error_text.text = error
        error_text.visibility = View.VISIBLE
    }

    /*Called when password was changed*/
    override fun changed(pasword: String) {
        activity?.onBackPressed()
        activity?.makeToast("Password Changed")
    }

    /*Open Forget Password Fragment*/
    private fun forgetPassword() {
        callBack.clicked()
        val fragment = ForgetPasswordFragment()
        fragment.setTargetFragment(this, FORGET_CODE)
        fragment.openFragment(
            fragmentManager = fragmentManager,
            openType = FOpenType.ADD,
            tag = FragmentTag.CHANGE_PASSWORD
        )
    }

    interface CommunicateCallBack {
        fun clicked()
    }
}