package com.example.autorizationtest.api

import com.example.autorizationtest.pojo.HeaderResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    /*Make Call to the Server for Temperature*/
    @GET("data/2.5/weather")
    fun getTemperature(@Query("lat") latitude: Double, @Query("lon") longitude: Double, @Query("appid") id: String): Call<HeaderResponse>
}