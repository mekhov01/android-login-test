package com.example.autorizationtest.api

import android.content.Context
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {

    private var retrofitJson: Retrofit? = null

    fun getClientJson(): Retrofit {
        if (retrofitJson == null) {
            val url = "http://api.openweathermap.org/"

            retrofitJson = Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofitJson!!
    }
}